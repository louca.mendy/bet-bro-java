package com.bet.bro.infra.postgres.entity;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.Hibernate;

@Entity
@Table(name = "team")
@Accessors(chain = true)
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Team {

  @Id
  @Column(name = "teamid", nullable = false)
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "acronym", nullable = false)
  private String acronym;

  @Column(name = "fullname", nullable = false)
  private String fullName;

  @Column(name = "creationdate", nullable = false)
  private LocalDateTime creationDate;

  @JoinColumn(name = "championshipid", nullable = false)
  @ManyToOne
  private Championship championship;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    Team team = (Team) o;
    return Objects.equals(id, team.id);
  }

  @Override
  public int hashCode() {
    return 0;
  }
}
