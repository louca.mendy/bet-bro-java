package com.bet.bro.infra.postgres.entity;

import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;
import lombok.experimental.Accessors;
import org.hibernate.Hibernate;

@Entity
@Table(name = "championship")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Accessors(chain = true)
public class Championship {

  @Id
  @Column(name = "championshipid", nullable = false)
  private Long id;

  @Column(name = "countryname", nullable = false)
  private String countryName;

  @Column(name = "leaguename", nullable = false)
  private String leagueName;

  @OneToMany(mappedBy = "championship")
  @Exclude
  private List<Team> teams;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    Championship that = (Championship) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return 0;
  }
}

