package com.bet.bro.infra.ws.championship;

import com.bet.bro.infra.postgres.entity.ChampionshipRepository;
import com.bet.bro.infra.postgres.entity.TeamEntityRepository;
import com.bet.bro.infra.ws.team.TeamsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HttpChampionshipBeanConfiguration {

  @Autowired
  TeamsMapper teamsMapper;

  @Autowired
  TeamEntityRepository teamsRepository;

  @Bean
  ChampionshipApplicationService championshipApplicationService(ChampionshipMapper championshipMapper,
      ChampionshipRepository championshipRepository) {
    return new ChampionshipApplicationService(championshipMapper, championshipRepository, teamsRepository);
  }

  @Bean
  ChampionshipMapper championshipMapper() {
    return new ChampionshipMapper(teamsMapper);
  }

}
