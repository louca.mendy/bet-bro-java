package com.bet.bro.infra.ws.team;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.openapitools.api.TeamsApi;
import org.openapitools.model.CreateTeamRequest;
import org.openapitools.model.TeamDescription;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TeamsEndpoints implements TeamsApi {

  private final TeamsApplicationService service;

  @Override
  public ResponseEntity<TeamDescription> createTeam(CreateTeamRequest createTeamRequest) {
    return ResponseEntity.status(HttpStatus.CREATED).body(service.createTeam(createTeamRequest));
  }

  @Override
  public ResponseEntity<List<TeamDescription>> getTeams() {
    TeamDescription teamDescription = new TeamDescription().acronym("SCO").fullName("SCO Angers");
    TeamDescription deux = new TeamDescription().fullName("Paris Saint Germain").acronym("PSG");
    TeamDescription trois = new TeamDescription().acronym("LOSC").fullName("Lille OSC");
    TeamDescription quatre = new TeamDescription().acronym("MHSC").fullName("Montpellier Hérault SC");

    List<TeamDescription> team = new ArrayList();
    team.add(teamDescription);
    team.add(deux);
    team.add(trois);
    team.add(quatre);

    return ResponseEntity.ok().body(team);
  }
}
