package com.bet.bro.infra.ws.team;

import com.bet.bro.infra.postgres.entity.ChampionshipRepository;
import com.bet.bro.infra.postgres.entity.TeamEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HttpTeamsBeanConfiguration {

  @Autowired
  ChampionshipRepository championshipRepository;

  @Bean
  TeamsApplicationService teamsApplicationService(TeamsMapper teamsMapper, TeamEntityRepository teamRepository) {
    return new TeamsApplicationService(teamsMapper, teamRepository);
  }

  @Bean
  TeamsMapper teamsMapper() {
    return new TeamsMapper(championshipRepository);
  }

}
