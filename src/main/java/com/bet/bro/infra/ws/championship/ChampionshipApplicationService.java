package com.bet.bro.infra.ws.championship;

import com.bet.bro.infra.postgres.entity.ChampionshipRepository;
import com.bet.bro.infra.postgres.entity.TeamEntityRepository;
import lombok.RequiredArgsConstructor;
import org.openapitools.model.ChampionshipsDescription;
import org.openapitools.model.ChampionshipsTeamsDescription;

@RequiredArgsConstructor
public class ChampionshipApplicationService {

  private final ChampionshipMapper mapper;
  private final ChampionshipRepository championshipRepository;
  private final TeamEntityRepository teamEntityRepository;

  public ChampionshipsDescription getChampionships() {
    return mapper.championshipDescription(championshipRepository.findAll());
  }

  public ChampionshipsTeamsDescription getTeamsChampionships() {
    return mapper.teamsChampionshipDescription(teamEntityRepository.findAll());
  }
}
