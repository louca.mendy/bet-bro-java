package com.bet.bro.infra.ws.championship;

import com.bet.bro.infra.postgres.entity.Championship;
import com.bet.bro.infra.postgres.entity.Team;
import com.bet.bro.infra.ws.team.TeamsMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openapitools.model.ChampionshipDescription;
import org.openapitools.model.ChampionshipsDescription;
import org.openapitools.model.ChampionshipsTeamsDescription;
import org.openapitools.model.TeamDescription;

@AllArgsConstructor
@Slf4j
public class ChampionshipMapper {

  private final TeamsMapper teamsMapper;

  public ChampionshipsDescription championshipDescription(List<Championship> championships) {
    return new ChampionshipsDescription().championships(
        championships.stream().map(this::to).collect(Collectors.toList()));
  }

  private ChampionshipDescription to(Championship championship) {
    return new ChampionshipDescription().championshipId(championship.getId()).leagueName(championship.getLeagueName())
        .countryName(
            championship.getCountryName());
  }

  public ChampionshipsTeamsDescription teamsChampionshipDescription(List<Team> championships) {
    Map<String, List<TeamDescription>> championshipsMap = new HashMap<>();
    List<TeamDescription> ligueUn = new ArrayList<>();
    List<TeamDescription> liga = new ArrayList<>();
    List<TeamDescription> pl = new ArrayList<>();

    getChampionships(championships, ligueUn, liga, pl);
    championshipsMap.put("Ligue1", ligueUn);
    championshipsMap.put("Liga", liga);
    championshipsMap.put("pl", pl);
    return new ChampionshipsTeamsDescription().championships(championshipsMap);
  }

  private void getChampionships(List<Team> championships, List<TeamDescription> ligueUn, List<TeamDescription> liga,
      List<TeamDescription> pl) {
    championships.forEach(t -> {
      switch (t.getChampionship().getLeagueName()) {
        case "Ligue 1":
          ligueUn.add(teamsMapper.teamDescription(t));
          break;
        case "La Liga":
          liga.add(teamsMapper.teamDescription(t));
          break;
        case "Premier League":
          pl.add(teamsMapper.teamDescription(t));
          break;
        default:
          log.error("Championship not found four : {}", t);
      }
    });
  }
}
