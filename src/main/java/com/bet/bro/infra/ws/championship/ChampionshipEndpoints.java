package com.bet.bro.infra.ws.championship;

import lombok.AllArgsConstructor;
import org.openapitools.api.ChampionshipApi;
import org.openapitools.model.ChampionshipsDescription;
import org.openapitools.model.ChampionshipsTeamsDescription;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ChampionshipEndpoints implements ChampionshipApi {

  private final ChampionshipApplicationService service;

  @Override
  public ResponseEntity<ChampionshipsDescription> getChampionships() {
    return ResponseEntity.status(HttpStatus.OK).body(service.getChampionships());
  }

  @Override
  public ResponseEntity<ChampionshipsTeamsDescription> getTeamsChampionship() {
    return ResponseEntity.ok(service.getTeamsChampionships());
  }

}
