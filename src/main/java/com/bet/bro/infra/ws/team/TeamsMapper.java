package com.bet.bro.infra.ws.team;

import com.bet.bro.infra.postgres.entity.ChampionshipRepository;
import com.bet.bro.infra.postgres.entity.Team;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openapitools.model.CreateTeamRequest;
import org.openapitools.model.TeamDescription;

@RequiredArgsConstructor
@Slf4j
public class TeamsMapper {

  private final ChampionshipRepository championshipRepository;

  public Team createTeam(CreateTeamRequest request) {
    log.info(request.toString());
    return new Team().setChampionship(championshipRepository.getOne(request.getChampionshipId()))
        .setAcronym(request.getAcronym()).setCreationDate(request.getCreationDate()).setFullName(
            request.getFullName());
  }

  public TeamDescription teamDescription(Team team) {
    return new TeamDescription().acronym(team.getAcronym()).fullName(team.getFullName()).creationDate(
        team.getCreationDate()).championshipId(team.getChampionship().getId());
  }
}
