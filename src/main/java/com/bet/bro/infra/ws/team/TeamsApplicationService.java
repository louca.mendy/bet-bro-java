package com.bet.bro.infra.ws.team;

import com.bet.bro.infra.postgres.entity.Team;
import com.bet.bro.infra.postgres.entity.TeamEntityRepository;
import lombok.RequiredArgsConstructor;
import org.openapitools.model.CreateTeamRequest;
import org.openapitools.model.TeamDescription;

@RequiredArgsConstructor
public class TeamsApplicationService {

  private final TeamsMapper mapper;
  private final TeamEntityRepository repository;

  public TeamDescription createTeam(CreateTeamRequest createTeamRequest) {

    final Team team = mapper.createTeam(createTeamRequest);
    final Team save = repository.save(team);
    return mapper.teamDescription(save);
  }
}
