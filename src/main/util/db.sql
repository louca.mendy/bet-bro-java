CREATE TABLE championship (
                        championshipid smallint NOT NULL,
                        countryname character varying(40),
                        leaguename character varying(40)
);

ALTER TABLE championship OWNER TO postgres;

CREATE TABLE team (
                              teamid smallint NOT NULL,
                              acronym character varying(40),
                              fullname character varying(40),
                              creationdate date,
                              championshipid smallint NOT NULL
);

ALTER TABLE championship OWNER TO postgres;

ALTER TABLE ONLY team
    ADD CONSTRAINT pk_team PRIMARY KEY (teamid);

ALTER TABLE ONLY championship
    ADD CONSTRAINT pk_championship PRIMARY KEY (championshipid);

ALTER TABLE ONLY team
    ADD CONSTRAINT fk_teams_championship FOREIGN KEY (championshipid) REFERENCES championship(championshipid);


INSERT INTO championship values (1,'France','Ligue 1') ;
INSERT INTO championship values (2,'Espagne','La Liga') ;
INSERT INTO championship values (3,'Angleterre','Premier League') ;